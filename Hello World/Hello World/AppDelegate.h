//
//  AppDelegate.h
//  Hello World
//
//  Created by Anshima on 23/01/17.
//  Copyright © 2017 Khats. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

