//
//  ViewController.h
//  Hello World
//
//  Created by Anshima on 23/01/17.
//  Copyright © 2017 Khats. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *projectLabel;

-(IBAction)clicked:(id)buttonId;

@end

