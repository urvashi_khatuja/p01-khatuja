//
//  ViewController.m
//  Hello World
//
//  Created by Anshima on 23/01/17.
//  Copyright © 2017 Khats. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize projectLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)clicked:(id)buttonId
{
    [projectLabel setText:@"Urvashi Khatuja" ];
    [projectLabel setTextColor:[UIColor redColor]];
    [projectLabel setFont:[UIFont fontWithName:@"Arial" size:20]];
}

@end
